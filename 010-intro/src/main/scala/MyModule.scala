
import fpinscala.Exercises.fib
// Andrzej Wąsowski, IT University of Copenhagen

object MyModule {

  def abs(n: Int): Int = if (n < 0) -n else n

  /* Exercise 1 */
  def square (n: Int): Int = n * n

  def fib (n: Int) : Int = {
    @annotation.tailrec
    def go(i: Int, prev: Int = 0, next: Int = 1) : Int =
      if(0 >= i) prev
      else if(1 >= i) next
      else go(i - 1, next, (next+prev))
    go(n)

  }

  def exNine() : String = {
    List(1,2,3,4,5) match {
      case ::(x, ::(2, ::(4, _))) => "case1"
      case Nil => "case 2"
      //case ::(x, ::(y, ::(3, ::(4, z)))) => "case 3"
      //case ::(h, t) => "case 4"
      case _ => "case5"
    }
  }

  def tail[A] (as: List[A]) : List[A] = as match {
      case Nil =>  sys.error("Nope")
      case ::(t, h) => h
  }

  def drop[A](l: List[A], n: Int): List[A] = {
    if(n < 1) l
    l match {
        case Nil => sys.error("Can't drop that many!")
        case ::(t, h) => drop(h, n-1)
    }
  }

  /*def dropWhile[A](l: List[A], f: A =>Boolean): List[A] = {
    if(!f()) l
  }*/

  private def formatAbs(x: Int) =
    s"The absolute value of $x is ${abs (x)}"

  val magic :Int = 42
  var result :Option[Int] = None


  def main(args: Array[String]): Unit = {
    assert (magic - 84 == magic.-(84))
    println (formatAbs (magic-100))
  }
}
